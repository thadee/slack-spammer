Slack Spammer
=============

## installation:

- run npm install to install dependencies
- if necessary, adjust default values in config.json
- run on cli: node index.js

## Command line options:
	--message={{ message body }}
	--username={{ from name }}
	--url={{ endpoint url }}
	--channel={{ #channel or @username to post to }}
	--iconurl={{ url to custom icon }}

make sure you quote strings you pass as command line option values if there are spaces in there

## enjoy spamming your colleagues!