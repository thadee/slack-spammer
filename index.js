var request = require('request');
var Q = require('q');
var minimist = require('minimist');
var args = minimist(process.argv.slice(2));
var fs = require('fs');

function getUser(alias, users) {
	for(var username in users){
		if(users.hasOwnProperty(username) && users[username].indexOf(alias) >= 0){
			return username;
		}
	}
	return alias;
}
function getPostObject(defaults) {
	return {
		form:{
			payload:JSON.stringify(
				{
					text: args.message || defaults.message,
					channel: getUser(args.channel, defaults.aliases) || defaults.channel,
					username: args.username || defaults.username,
					icon_url: args.iconurl || defaults.icon_url
				}
			)
		}
	}
}
/**
 * Send a message to someone on slack
 *
 * command line options:
 *
 * --message={{ message body }}
 * --username={{ from name }}
 * --url={{ endpoint url }}
 * --channel={{ #channel or @username to post to }}
 * --iconurl={{ url to custom icon }}
 */
module.exports = (function (args) {

	Q.nfcall(fs.readFile, __dirname + '/config.json')
		.then(function readConfig(file) {
			var config = JSON.parse(file),
				post = getPostObject(config),
				url = args.url || config.url;

			return Q.nfcall(request.post, url, post)
				.then(function handlePostResponse(response) {
					var msg = response[response.length - 1];
					return msg === 'ok' ? 'transmission ' + msg : 'ERROR: ' + msg;
				});
		})
		.then(console.log,function (error) {
			console.error('Error!', error);
		});
}(args));